package com.urlinfo.urlinfo;




import static org.mockito.ArgumentMatchers.any;
import static org.hamcrest.Matchers.hasSize;

import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;

public class UrlinfoApplicationTests {
	private List<UrlEntity> database;
	private UrlController rest;
	private MockMvc endpoint;
	private UrlRepository repo;

	@Before
	public void setup() {
		database = new ArrayList<>();
		repo = mock(UrlRepository.class);
		rest = new UrlController(repo);
		endpoint = standaloneSetup(rest).build();
	}

	@Test
	public void create_ok() throws Exception {

		doAnswer(i -> {
			final UrlEntity url = i.getArgument(0);
			database.add(url);
			return url;
		}).when(repo).save(any());

		doAnswer(i -> database).when(repo).findAll();


		endpoint
				.perform(post("/urladd").contentType(APPLICATION_JSON).content("{\"url\": \"localhost1\" , \"port\" : \"8080\"}"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$").value(hasSize(1)))
				.andExpect(jsonPath("$[0]").value("localhost1:8080"));

		endpoint
				.perform(post("/urladd").contentType(APPLICATION_JSON).content("{\"url\": \"localhost2\" , \"port\" : \"8080\" }"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$").value(hasSize(2)))
				.andExpect(jsonPath("$[0]").value("localhost1:8080"))
				.andExpect(jsonPath("$[1]").value("localhost2:8080"));
	}

	@Test
	public void create_badRequest() throws Exception {
		endpoint
				.perform(post("/urladd").contentType(APPLICATION_JSON).content("{\"typoName\": \"localhost\"}"))
				.andExpect(status().isBadRequest())
				.andExpect(content().string("'url or port' is missing."));
	}


}
