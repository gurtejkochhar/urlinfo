package com.urlinfo.urlinfo;

public class UrlData {

    private final String response;

    public UrlData( String response) {

        this.response = response;
    }

    public String getResponse() {
        return response;
    }
}
