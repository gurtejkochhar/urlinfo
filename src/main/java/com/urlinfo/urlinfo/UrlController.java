package com.urlinfo.urlinfo;
import java.util.concurrent.atomic.AtomicLong;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.ResponseEntity.badRequest;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class UrlController {

    private static final String template = "Url %s is , %s!";
    private final AtomicLong counter = new AtomicLong();
    //private static  Iterable<String> URLS = new ArrayList<>();

    private final UrlRepository repo;

    @Autowired
    public UrlController(final UrlRepository repo) {
        this.repo = repo;
    }

    @RequestMapping("/urlinfo")
    public UrlData urldata(@RequestParam(value="url") String urlstr,
			   @RequestParam(value="port") String port) {
        for (String curVal : flattenUrlList()){
	    
            if (curVal.equals(urlstr+":"+port)){
                return new UrlData(
                        String.format(template, (urlstr+":"+port), "safe"));
            }
        }
        return new UrlData(
                String.format(template, (urlstr+":"+port), "unsafe"));
    }


    @RequestMapping(path = "/urladd", method = POST)
    public ResponseEntity<?> writeDownUrl(final @RequestBody Map<String, Object> input) {
        if (isInputValid(input)) {
            saveUrl(input);
            return ok(flattenUrlList());
        }
        else
            return badRequest().body("'url or port' is missing.");
    }

    private boolean isInputValid(final Map<String, Object> input) {
        return (input.containsKey("url") && input.get("url")!=null) && (input.containsKey("port") && input.get("port") != null);
    }

    private Iterable<String> flattenUrlList() {
        final List<String> urlList = new ArrayList<>();
        final Iterable<UrlEntity> urls = repo.findAll();
        urls.forEach(v -> urlList.add(v.getUrl()+":"+v.getPort()));
        return urlList;
    }

    private void saveUrl(final Map<String, Object> input) {
        final UrlEntity url = new UrlEntity();
        url.setId(UUID.randomUUID().toString());
        url.setUrl(input.get("url").toString());
	    url.setPort(input.get("port").toString());
        repo.save(url);
    }
}
