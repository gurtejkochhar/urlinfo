package com.urlinfo.urlinfo;

import org.springframework.data.annotation.Id;

public class UrlEntity {

    @Id
    private String id;
    private String url_add;
    private String port;

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getUrl() {
        return url_add;
    }

    public void setUrl(final String url_add) {
        this.url_add = url_add;
    }

    public String getPort() {
        return port;
    }

    public void setPort(final String port) {
        this.port = port;
    }
}
