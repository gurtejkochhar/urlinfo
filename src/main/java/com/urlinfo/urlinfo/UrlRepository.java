package com.urlinfo.urlinfo;

import org.springframework.data.repository.CrudRepository;

public interface UrlRepository extends CrudRepository<UrlEntity, String>{
}
