#FROM fabric8/java-alpine-openjdk8-jdk
FROM registry.access.redhat.com/ubi8/ubi-minimal

ENV JAVA_MAJOR_VERSION=8 \
    JAVA_HOME=/usr/lib/jvm/jre-1.8.0

RUN microdnf install java-1.8.0-openjdk-headless \
  && microdnf clean all
#ENV MONGO_URL mongodb://localhost/urlinfo_01
EXPOSE 8080
RUN mkdir -p /app/
ADD build/libs/urlinfo-0.0.1-SNAPSHOT.jar /app/urlinfo-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java", "-jar", "/app/urlinfo-0.0.1-SNAPSHOT.jar"]
